#!/bin/busybox sh
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

#
# * * * EXPERIMENTAL * * *
# Experimental /init script for use in the hypervisor initramfs.
#

is_cros_debug() {
  grep -qw cros_debug /proc/cmdline 2>/dev/null
}

# Prepare standard paths and mount points.
setup_environment() {
  # Install additional utility programs.
  /bin/busybox --install /bin || true

  mount -n -t sysfs -o nodev,noexec,nosuid sysfs /sys
  mount -n -t pstore -o nodev,noexec,nosuid pstore /sys/fs/pstore
  mount -n -t proc -o nodev,noexec,nosuid none /proc
  mount -n -t devtmpfs -o mode=0755,nosuid devtmpfs /dev

  mkdir /dev/pts
  mount -n -t devpts -o noexec,nosuid,mode=0600 devpts /dev/pts

  # Disable lowmem reserve. The only thing in the hypervisor that makes
  # GFP_DMA32 allocations is the gsmi driver, and it has already made its
  # allocations. Reserving part of ZONE_DMA32 for GFP_DMA32 allocations at
  # this point would just be wasting memory.
  echo "0 0 0" > /proc/sys/vm/lowmem_reserve_ratio
}

# Calculate how much memory to give the CrOS guest
get_cros_guest_memory() {
  # Reserve 512MB of total system memory for hypervisor.
  local mem_total=$(echo "$1" | grep MemTotal)
  local mem_kb=${mem_total//[^0-9]/}
  local mem_mb=$((mem_kb/1024 - 512))
  echo "${mem_mb}"
}

MMS_BRIDGE="/run/mms-bridge"
APPS_MEMCG="apps"
APPS_MEMCG_INIT_MB="16"

# TODO: use inotify
wait_for_socket() {
  while [ ! -S "$1" ]; do sleep .05; done
}

# Start ManaTEE Monitor.
run_hypervisor_services() {
  echo $$ > "/sys/kernel/cgroup/${APPS_MEMCG}/cgroup.procs"
  /sbin/trichechus "-M" "$MMS_BRIDGE" --init &
  echo $$ > /sys/kernel/cgroup/cgroup.procs

  wait_for_socket "$MMS_BRIDGE"
  wait_for_socket "/dev/log"

  /sbin/manatee_memory_service "$1" "$MMS_BRIDGE" \
      "$APPS_MEMCG" "$APPS_MEMCG_INIT_MB" &
  wait_for_socket "/run/mms_control_0.sock"
}

# add pcieport and vfio-pci driver into allowlist
setup_pci_allowlist() {
  if [[ -w "/sys/bus/pci/drivers_allowlist_lockdown" ]]; then
    echo "pcieport" > /sys/bus/pci/drivers_allowlist
    echo "vfio-pci" > /sys/bus/pci/drivers_allowlist
    echo "vfio-pci-pm" > /sys/bus/pci/drivers_allowlist
    echo 0 > /sys/bus/pci/drivers_allowlist_lockdown
  fi
}

setup_memcg() {
  mount -t cgroup2 none /sys/kernel/cgroup/
  echo "+memory" > /sys/kernel/cgroup/cgroup.subtree_control
  mkdir "/sys/kernel/cgroup/${APPS_MEMCG}"
}

# Executes a shell on the serial console.
run_shell() {
  if is_cros_debug; then
    echo $$ > "/sys/kernel/cgroup/${APPS_MEMCG}/cgroup.procs"
    if [[ "$1" == "bg" ]]; then
      # NB: cttyhack forces the shell to run on the serial console
      # even though it's started in a background job.
      setsid cttyhack sh &
    else
      setsid cttyhack sh
    fi
    echo $$ > "/sys/kernel/cgroup/cgroup.procs"
  fi
}

# Initialize serial port according to hardware status and kernel command line.
#
# The manatee_serial kernel command line option (which the kernel passes in the
# environment when it runs this script as init) indicates what to run on the
# serial port. Choices include:
#   off              - do not use the serial port even if present
#   os               - (default) serial port hosts  emulated serial port for OS
#   shell            - run a busybox shell on the hypervisor side
#   foreground_shell - Run a shell in the foreground, don't launch any other
#                      jobs. Useful for experimenting with customized crosvm
#                      command lines etc.
setup_serial() {
  if [[ -z "${manatee_serial}" ]]; then
    export manatee_serial=os
  fi

  if [[ "${manatee_serial}" != "off" && \
        -r /sys/class/tty/ttyS0/type && \
        "$(cat /sys/class/tty/ttyS0/type)" != 0 ]]; then
    # Make sure ttyS0 is configured correctly in case the kernel didn't do it.
    exec <>/dev/ttyS0 1>&0 2>&0
    stty 115200 cs8 -cstopb -parenb
  else
    # No serial port, disable any further handling.
    manatee_serial=off
    exec <>/dev/null 1>&0 2>&0
  fi

  case "${manatee_serial}" in
    shell)
      run_shell "bg"
      ;;
    foreground_shell)
      cat <<EOM
Welcome to the ManaTEE debug shell. It's useful for inspecting ManaTEE
hypervisor userspace and experimentation, e.g. to invoke the OS VM differently.
Note that the ManaTEE jobs aren't started automatically, but you can do so as
follows:

 # launch the dispatcher / OS interface in the background
 trichechus -M /run/mms-bridge &
 # launch the memory management service
 manatee_memory_service <CrOS guest MiB> /run/mms-bridge &
 # launch the OS guest
 crosvm-direct.sh <CrOS guest MiB>

Closing this shell will result in init terminating, causing a kernel panic,
which in turn leads to a reboot.

Have fun!
EOM
      run_shell "fg"
      exit 1
      ;;
  esac
}

# Main.
main() {
  # cbmem needs /dev/mem, so just create that in an effort to collect the first
  # hypervisor user space time stamp as early as possible.
  busybox mknod /dev/mem c 1 1
  cbmem -a TS_CRHV_BOOT

  setup_environment

  setup_pci_allowlist

  setup_memcg

  setup_serial

  cbmem -a TS_CRHV_PLATFORM_INIT

  local cros_guest_memory="$(get_cros_guest_memory "$(cat /proc/meminfo)")"

  # Start hypervisor services.
  run_hypervisor_services "${cros_guest_memory}"

  cbmem -a TS_CRHV_SERVICES_STARTED

  # Start ManaTEE Primary VM.
  /bin/crosvm-direct.sh "${cros_guest_memory}"
  rc="$?"
  case "${rc}" in
    0)
      poweroff -f
      ;;
    32)
      reboot -f
      ;;
    34)
      reboot -f
      ;;
  esac

  # Echo exit code in case someone is watching the console
  echo Unexpected crosvm exit code: "${rc}"
  reboot -f
}

# Make this source-able for testing.
if [ "$0" = "/init" ]; then
  main "$@"
  # This script is running as init, exiting results in kernel panic and reboot.
  exit 1
fi
